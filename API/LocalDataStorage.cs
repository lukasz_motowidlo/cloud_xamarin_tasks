﻿using System.Collections.Generic;
using System.Linq;
using DataContracts;

namespace API
{
    public class LocalDataStorage
    {
        private readonly List<Photo> _photos = new List<Photo>();
        public IReadOnlyCollection<Photo> Photos => _photos;

        public void AddPhoto(Photo p)
        {
            p.Id = _photos.OrderByDescending(by => by.Id).FirstOrDefault()?.Id + 1 ?? 1;
            _photos.Add(p);
        }
    }
}