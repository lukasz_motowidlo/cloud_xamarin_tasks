﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using DataContracts;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhotosController : ControllerBase
    {
        private readonly LocalDataStorage _data;

        public PhotosController(LocalDataStorage data)
        {
            _data = data;
        }

        [HttpGet]
        public IActionResult GetPeople()
        {
            return Ok(_data.Photos);
        }

        [HttpPost]
        public IActionResult AddPerson([FromBody] Photo photo)
        {
            _data.AddPhoto(photo);
            return Ok();
        }

        [HttpGet("{id}/photo")]
        public IActionResult GetPhoto([FromRoute] int id)
        {
            var p = _data.Photos.First(w => w.Id == id);
            return base.File(Convert.FromBase64String(p.PhotoBase64), "image/jpeg");
        }
    }
}