﻿using System;
using System.ComponentModel;
using System.IO;
using DataContracts;
using Xamarin.Forms;

namespace cloud_xamarin_tasks
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private readonly IPhotoClient _client;
        private Photo _photo = new Photo();

        public MainPage(IPhotoClient client)
        {
            InitializeComponent();
            InitializeEvents();
            _client = client;
        }

        /// <summary>
        /// 
        /// </summary>
        private async void InitializeEvents()
        {
            btnCamera.Clicked += async (sender, e) =>
            {
                var photo = await Plugin.Media.CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
                {

                });

                if (photo != null)
                {
                    imgPhoto.Source = ImageSource.FromStream(() => photo.GetStream());
                    byte[] bytes;
                    using (var memoryStream = new MemoryStream())
                    {
                        await photo.GetStream().CopyToAsync(memoryStream);
                        bytes = memoryStream.ToArray();
                    }

                    string base64 = Convert.ToBase64String(bytes);
                    _photo.PhotoBase64 = base64;
                }
            };

            btnSave.Clicked += async ( sender,  e) =>
            {
                if (string.IsNullOrEmpty(_photo.PhotoBase64))
                {
                    await DisplayAlert("Validation Error", "Photo wasnt taken.", "Ok");
                    return; 
                }
                try
                {
                    await _client.AddPhotoAsync(_photo);
                    await DisplayAlert("Success", "Data has been saved.", "Ok");
                }
                catch (Exception ex)
                {
                    await DisplayAlert("Error", ex.Message, "Ok");
                }
            };
        }
       
    }
}
