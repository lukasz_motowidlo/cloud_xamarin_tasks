﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using DataContracts;
namespace cloud_xamarin_tasks
{
    public partial class App : Application
    {
        public App()
        {
            var client = RestEase.RestClient.For<IPhotoClient>("https://192.168.1.13:5001/api");

            InitializeComponent();

            MainPage = new MainPage(client);
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
