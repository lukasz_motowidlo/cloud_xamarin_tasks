﻿using System.Threading.Tasks;
using RestEase;

namespace DataContracts
{
    public interface IPhotoClient
    {
        [Post("photos")]
        Task AddPhotoAsync([Body] Photo person);
    }
}
