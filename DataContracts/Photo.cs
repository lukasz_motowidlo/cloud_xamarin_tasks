﻿using System;

namespace DataContracts
{
    public class Photo
    {
        public int Id { get; set; }
        public string PhotoBase64 { get; set; }
    }
}
